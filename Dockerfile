FROM eclipse-temurin:8-jdk

# Set Target Versions
ENV ANDROID_COMPILE_SDK 29
ENV ANDROID_BUILD_TOOLS 29.0.3
ENV ANDROID_SDK_TOOLS 6858069
ENV ANDROID_NDK_VERSION 26.3.11579264

# Set Paths
ENV PATH="${PATH}:${PWD}/android-sdk-linux/platform-tools/"
ENV ANDROID_HOME="${PWD}/android-sdk-linux"
ENV NDK_ROOT="${PWD}/android-sdk-linux/ndk/${ANDROID_NDK_VERSION}"

# Install updates and necessary packages for build
RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y \
	cmake \
	make \
	bsdmainutils \
	curl \
	xxd \
	autotools-dev \
	autoconf \
	automake \
	pkg-config \
	perl \
	git patch \
	gcc g++ \
	libtool \
	python3 \
	python3-pip \
	ruby-full \
	gnupg \
	ca-certificates \
	sudo \
 && rm -rf /var/lib/apt/lists/* \
 && gem install rake --no-document \
 && gem install fastlane --no-document

# Install Android SDK and components necessary for libretro
RUN wget -q -O android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip \
 && unzip -d android-sdk-linux android-sdk.zip \
 && rm -f android-sdk.zip \
 && mkdir -p ~/.android/ && touch ~/.android/repositories.cfg \
 && mkdir android-sdk-linux/cmdline-tools/latest \
 && find android-sdk-linux/cmdline-tools -maxdepth 1 -mindepth 1 -not -name latest -print0 | xargs -0 mv -t android-sdk-linux/cmdline-tools/latest/ \
 && echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" \
 && echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platform-tools" \
 && echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" \
 && echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "ndk;${ANDROID_NDK_VERSION}" \
 && yes | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager --licenses
